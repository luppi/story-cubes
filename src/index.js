const CUBES = {
classic_orig:[
    ['Magnet','Sheep','Lock','Learner','Driver Sign','Footprint Flame'],
    ['Tower','Pyramid','Mail','Rainbow','Eye','Tree'],
    ['Light Bulb','House with Chimney','Speech Bubble','Snore','Bow and Arrow','Clock(4oclock)'],
    ['Sunflower','Keyhole','Compass','Man Fallingin Parachute','Striped Fish','Comedy/Tragedy Masks'],
    ['Smile','Airplane','Flashlight','Skyscraper','Earth','Apple'],
    ['Hand','Dice','Magnifying Glass','Scarab Beetle','Spooky Shadow','Turtle'],
    ['Key',	'Frown',	'Question Mark',	'Fountain',	'Shooting Star', 'Teepee'],
    ['Lightning Bolt',	'Balance Scales',	'Diagional Arrow',	'Cane',	'Cell Phone',	'Grey Alien'],
    ['Bee',	'Bridge over water',	'Abacus',	'Open Book',	'Waxing Crescent Moon',	'Magic Wand with Stars']
  ],
  classic:[
    ['Magnet','Sheep','Lock','Student','Driver Sign','Footprint Flame'],
    ['Tower','Pyramid','Mail','Rainbow','Eye','Tree'],
    ['Light Bulb','House','Speech Bubble','Snore','Bow and Arrow','Clock'],
    ['Sunflower','Keyhole','Compass','Parachute','Fish','Mask'],
    ['Smile','Airplane','Flashlight','Skyscraper','Earth','Apple'],
    ['Hand','Dice','Magnifying Glass','Beetle','Spooky Shadow','Turtle'],
    ['Key',	'Frown',	'Question Mark',	'Fountain',	'Shooting Star', 'Teepee'],
    ['Lightning Bolt',	'Balance Scales',	'Diagional Arrow',	'Cane',	'Cell Phone',	'Grey Alien'],
    ['Bee',	'Bridge over water',	'Abacus',	'Open Book',	'Crescent Moon',	'Magic Wand']
  ],
  actions:[
    ['Reading a Book','Investigating Gift',	'Blindfolded Face Feeling',	'Accepting or Giving Gift',	'Lost Tooth',	'90 Degree Turn'],
    ['Laugh',	'Choosing a Direction',	'Hanging during a Pullup',	'Raise Hand to Speak',	'Throw Ball in Air',	'Bomb Landing on Another Planet'],
    ['Butterfly & Net',	'Eat',	'Climb',	'Friendship (Thumbs Up)',	'Counting Coins',	'Kick'],
    ['Ball Bounce',	'Cough or Ahem!',	'Leading the Way',	'Walking',	'Weight Lifter',	'Walking with Heavy Box'],
    ['Dig',	'Dance',	'Fill in Dug Spot',	'Yell',	'Fight',	'Break'],
    ['Baseball Hit',	'Headphones',	'Play with Dolls',	'Enter Doorway',	'Fall',	'Didn\'t Mean To See That'],
    ['Thinking',	'Push Button',	'Attempting to push over heavy object',	'Noticed Ball Dropping Behind You',	'Thief Getting Away',	'Match & Burning Paper'],
    ['Scissors & Paper',	'Cry',	'Drop',	'Tip Over',	'Hop Down',	'Draw'],
    ['Look over there',	'Reach',	'Knock',	'Alarm Clock',	'Brick Builder',	'Drying Clothes on Wire']
  ]
}

// This will be the array to hold all of the decided on story cubes.
let story = []

// calculate the story cubes
for(let x = 0; x < CUBES.classic.length; x++ ) {
  let side = Math.floor(Math.random() * 6)
  story.push(CUBES.classic[x][side])
}

// and display them in the UI
document.getElementById("title").innerHTML = `Your story must contain the followin elements:</br></br>${story.join('</br>')}`;
